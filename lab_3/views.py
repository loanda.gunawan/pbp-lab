from django.http import response
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from lab_1.models import Friend
from .forms import FriendForm


@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()
    context = {'friends': friends}
    return render(request, 'lab3_index.html', context)


@login_required(login_url='/admin/login/')
def add_friend(request):
    if request.method != 'POST':
        form = FriendForm()
    else:
        form = FriendForm(request.POST or None)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-3')
    
    context = {'form': form}
    return render(request, 'lab3_form.html', context)
