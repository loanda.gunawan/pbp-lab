import 'package:flutter/material.dart';
import 'package:collection/collection.dart';

class Home extends StatefulWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final myController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  double nilaiSlider = 0;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;

  String header =
      "Dalam 2 minggu terakhir, seberapa sering Anda terganggu oleh masalah-masalah berikut?";

  List<String> questions = [
    "Kurang berminat atau bergairah dalam melakukan apapun",
    "Merasa murung, sedih, atau putus asa",
    "Sulit tidur/mudah terbangun, atau terlalu banyak tidur",
    "Merasa lelah atau kurang bertenaga",
    "Kurang nafsu makan atau terlalu banyak makan",
    "Kurang percaya diri — atau merasa bahwa Anda adalah orang yang gagal atau telah mengecewakan diri sendiri atau keluarga",
    "Sulit berkonsentrasi pada sesuatu, misalnya membaca koran atau menonton televisi",
    "Bergerak atau berbicara sangat lambat sehingga orang lain memperhatikannya. Atau sebaliknya; merasa resah atau gelisah sehingga Anda lebih sering bergerak dari biasanya",
    "Merasa lebih baik mati atau ingin melukai diri sendiri dengan cara apapun"
  ];

  List<Map<String, Object>> choices = [
    {
      'text': 'Tidak pernah',
      'score': 0,
    },
    {
      'text': 'Beberapa hari',
      'score': 1,
    },
    {
      'text': 'Lebih dari seminggu',
      'score': 2,
    },
    {
      'text': 'Hampir setiap hari',
      'score': 3,
    },
  ];

  late List<double> scores = [for (var i = 0; i < questions.length; i++) 0];

  String get resultText {
    String resultText;
    int totalScore = scores.sum as int;
    if (totalScore >= 20) {
      resultText = 'Depresi berat';
    } else if (totalScore >= 15) {
      resultText = 'Depresi sedang';
    } else if (totalScore >= 10) {
      resultText = 'Depresi ringan';
    } else if (totalScore >= 5) {
      resultText = 'Gejala depresi ringan';
    } else {
      resultText = 'Tidak terdapat gejala depresi';
    }
    return resultText;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Lab 7"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: myController,
                    decoration: InputDecoration(
                      hintText: "contoh: Kambing Gunawan",
                      labelText: "Nama Lengkap",
                      icon: const Icon(Icons.people),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0)
                      ),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Nama tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(header),
                ),
                for (int i = 0; i < questions.length; i++)
                Column(
                  children: [
                    const Divider(
                      thickness: 2.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(questions[i]),
                    ),
                    Slider(            
                      value: scores[i],
                      min: 0,
                      max: 3,
                      divisions: 3,
                      label: '${choices[scores[i].round()]['text']}',
                      onChanged: (value) {
                        setState(() {
                          scores[i] = value;
                        });
                      },
                    ),
                  ],
                ),
                
                ElevatedButton(
                  child: const Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  style: ElevatedButton.styleFrom(
                    primary: Colors.blue,
                  ),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      showDialog(
                        context: context, 
                        builder: (context) {
                          return AlertDialog(
                            title: Text("Hasil deteksi depresi [${myController.text}]"),
                            content: Text(resultText),
                          );
                        },
                      );
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}