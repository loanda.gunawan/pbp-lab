import 'package:flutter/material.dart';
import 'package:lab_6/screens/deteksi_depresi_phq9.dart';

class PHQ9Result extends StatelessWidget {
  final int totalScore;

  PHQ9Result({
    required this.totalScore,
  });

  String get resultText {
    String resultText;
    if (totalScore >= 20) {
      resultText = 'Depresi berat';
    } else if (totalScore >= 15) {
      resultText = 'Depresi sedang';
    } else if (totalScore >= 10) {
      resultText = 'Depresi ringan';
    } else if (totalScore >= 5) {
      resultText = 'Gejala depresi ringan';
    } else {
      resultText = 'Tidak terdapat gejala depresi';
    }
    return resultText;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('PHQ9 Result'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Total skor $totalScore',
              style: const TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ), //Text
            Text(
              resultText,
              style: const TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text('Kembali'),
            ),
          ],
        ),
      ),
    );
  }
}
