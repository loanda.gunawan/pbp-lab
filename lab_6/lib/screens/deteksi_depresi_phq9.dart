import 'package:flutter/material.dart';
import 'package:collection/collection.dart';
import 'package:lab_6/screens/deteksi_depresi_result.dart';

class PHQ9 extends StatefulWidget {
  const PHQ9({ Key? key }) : super(key: key);


  @override
  _PHQ9State createState() => _PHQ9State();
}

class _PHQ9State extends State<PHQ9> {

  String header = "Dalam 2 minggu terakhir, seberapa sering Anda terganggu oleh masalah-masalah berikut?";

  List<String> questions = [
    "Kurang berminat atau bergairah dalam melakukan apapun",
    "Merasa murung, sedih, atau putus asa",
    "Sulit tidur/mudah terbangun, atau terlalu banyak tidur",
    "Merasa lelah atau kurang bertenaga",
    "Kurang nafsu makan atau terlalu banyak makan",
    "Kurang percaya diri — atau merasa bahwa Anda adalah orang yang gagal atau telah mengecewakan diri sendiri atau keluarga",
    "Sulit berkonsentrasi pada sesuatu, misalnya membaca koran atau menonton televisi",
    "Bergerak atau berbicara sangat lambat sehingga orang lain memperhatikannya. Atau sebaliknya; merasa resah atau gelisah sehingga Anda lebih sering bergerak dari biasanya",
    "Merasa lebih baik mati atau ingin melukai diri sendiri dengan cara apapun"
  ];

  List<Map<String, Object>> choices = [
    {
      'text': 'Tidak pernah',
      'score': 0,
    },
    {
      'text': 'Beberapa hari',
      'score': 1,
    },
    {
      'text': 'Lebih dari seminggu',
      'score': 2,
    },
    {
      'text': 'Hampir setiap hari',
      'score': 3,
    },
  ];

  late List<int> scores = [
    for (var i = 0; i < questions.length; i++) 0
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Deteksi Dini Depresi - PHQ9'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(3.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Table(
                border: TableBorder.all(
                  color: Colors.black,
                  width: 1,
                ),
                children: [
                  TableRow(
                    decoration: const BoxDecoration(color: Colors.black),
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          header,
                          style: const TextStyle(color: Colors.white),
                          textScaleFactor: 1.2,
                        ),
                      ),
                    ],
                  ),
                  for (int i = 0; i < questions.length; i++)
                  TableRow(
                    children: [
                      Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Text(
                              questions[i],
                              textScaleFactor: 1.2,
                            ),
                          ),
                          for (Map<String, Object> choice in choices)
                          ListTile(
                            title: Text(choice['text'] as String),
                            leading: Radio<int>(
                              value: choice['score'] as int,
                              groupValue: scores[i],
                              onChanged: (value) {
                                setState(() {
                                  scores[i] = value as int;
                                });
                              },
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              Center(
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => PHQ9Result(totalScore: scores.sum,)),
                    );
                  }, 
                  child: const Text('Lihat hasil'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}