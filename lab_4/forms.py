from django import forms
from lab_2.models import Note


class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['to', 'frm', 'title', 'message']
        labels = {
            'frm': 'From'
        }
        widgets = {
            'to': forms.TextInput(attrs={'class': 'form-control'}),
            'frm': forms.TextInput(attrs={'class': 'form-control'}),
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'message': forms.TextInput(attrs={'class': 'form-control'}),
        }
