from django.http.response import HttpResponse
from django.shortcuts import render
from django.core import serializers
from django.http.response import HttpResponse

from .models import Note

# Return Note in HTML format
def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab2.html', response)

# Return Note in XML format
def xml(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

# Return Note in JSON format
def json(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")

